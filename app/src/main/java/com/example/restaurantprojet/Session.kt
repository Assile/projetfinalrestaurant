package com.example.restaurantprojet

import android.content.Context
import android.content.SharedPreferences

class Session constructor(context: Context){
     var context:Context?=null
    var preferenceName="user"
     var  prefs=context.getSharedPreferences(preferenceName,Context.MODE_PRIVATE);
    init {
        this.context=context

    }

    fun setUserRef(user: String?) {
val editor=prefs.edit()
    editor.putString("user",user)
        editor.apply()
    }

    fun getUserRef(): String? {
        return prefs.getString("user", "")
    }


}