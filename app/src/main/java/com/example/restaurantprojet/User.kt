package com.example.restaurantprojet

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    var nom: String?,
    var prenom: String?,
    var user: String?,
    var pw: String?,
    var mail: String?,
    var photo: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        nom =parcel.readString(),
        prenom =parcel.readString(),
        user =parcel.readString(),
        pw =parcel.readString(),
        mail =parcel.readString(),
        photo =parcel.readString()
    ) {
    }

    companion object : Parceler<User> {

        override fun User.write(parcel: Parcel, flags: Int) {
            parcel.writeString(nom)
            parcel.writeString(prenom)
            parcel.writeString(user)
            parcel.writeString(pw)
            parcel.writeString(mail)
            parcel.writeString(photo)
        }

        override fun create(parcel: Parcel): User {
            return User(parcel)
        }
    }
}