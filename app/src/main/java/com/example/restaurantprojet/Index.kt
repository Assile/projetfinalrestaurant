package com.example.restaurantprojet

import android.Manifest
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.circularimageview.CircularImageView
import java.io.IOException
import java.util.*

class Index : AppCompatActivity() {
    private var btnShowLocation: Button? = null
    private var gps: GpsLocationTracker? = null



     var RestListView: ListView? = null
    val queue = MySingleton.getInstance(this).requestQueue

    var context: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_index)

        var user: User? =intent.getParcelableExtra("User")
        //val image: CircularImageView = findViewById(R.id.profilIv) as CircularImageView
        //var  route=context!!.getResources().getIdentifier("drawable/"+user?.photo, null, context!!.getPackageName());
       // var photo:com.blogspot.atifsoftwares.circularimageview.CircularImageView?=findViewById(R.id.profilIv) as CircularImageView
       // photo?.setImageResource(R.drawable.assile)


        var nomText:TextView?=findViewById(R.id.nameTv) as TextView
        nomText?.setText(user!!.nom+" "+user!!.prenom)
        var mailText:TextView?=findViewById(R.id.emailTv) as TextView
        mailText?.setText(user!!.mail)

        val geocoder = Geocoder(this)
        val button_gps = findViewById<ImageButton>(R.id.btn_gps)

        button_gps.setOnClickListener {
            gps = GpsLocationTracker(this@Index)

            if (gps!!.canGetLocation) {
                val latitude = gps!!.getLatitude()
                val longitude = gps!!.getLongitude()
                var addressList: List<Address>? = null
                try {
                    addressList = geocoder.getFromLocation(latitude, longitude, 1)
                } catch (e: IOException) {
                    e.printStackTrace()
                    println(e)
                }
                val pays = addressList!![0].countryName
                val province = addressList[0].adminArea
                var ville = addressList[0].locality

                var villeText:TextView?=findViewById(R.id.VilleTv) as TextView
                villeText?.setText(ville)





                var request = GetWayRestaurant(this, queue)


                RestListView = findViewById<View>(R.id.ListRestaurant) as ListView



                RestListView =
                    findViewById<View>(R.id.ListRestaurant) as ListView
                request.getAllRestaurant(ville, object : GetWayRestaurant.getAllCallback {
                    override fun onSuccess(listeOfRestaurant: ArrayList<Restaurant>) {

                        val adapter =
                            RestaurantAdapter(applicationContext, listeOfRestaurant)
                        RestListView!!.setAdapter(adapter)

                    }

                    override fun Onerror(message: String?) {}

                })
            }


        }
    }

    companion object {
        private val INITIAL_PERMS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    }






}
