package com.example.restaurantprojet
import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import java.util.*

class RestaurantAdapter(context: Context, Restaurant: ArrayList<Restaurant>) : BaseAdapter() {
    var context: Context
    var listRestaurant: List<Restaurant>? = null
    private val inflater: LayoutInflater? = null
    override fun getCount(): Int {
        return listRestaurant!!.size
    }

    override fun getItem(position: Int): Any {
        return listRestaurant!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ResourceAsColor")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        var viewItem: ViewItem? = null
        if (convertView == null) {
            viewItem = ViewItem()
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            Log.i("APP", "mmmm")
            convertView = layoutInflater.inflate(R.layout.row_resto, null)
            viewItem.nom =convertView.findViewById<View>(R.id.restoName) as TextView
            viewItem.emplacement =convertView.findViewById<View>(R.id.adressTv) as TextView
           viewItem.specialite =convertView.findViewById<View>(R.id.speciliteTv) as TextView



           // viewItem.note =convertView.findViewById<View>(R.id.noteBar) as RatingBar
            //viewItem.description =convertView.findViewById<View>(R.id.userList) as TextView
            viewItem.statut =convertView.findViewById<View>(R.id.restoClosedTv) as TextView
            viewItem.photo =convertView.findViewById<View>(R.id.RestoIv) as ImageView

            convertView.tag = viewItem
        } else {
            viewItem = convertView.tag as ViewItem
        }

        viewItem.nom!!.setText(this!!.listRestaurant!![position].nom)
       viewItem.emplacement!!.setText(listRestaurant?.get(position)?.adresse)
        viewItem.specialite!!.setText(listRestaurant?.get(position)?.specialite)
        if ("Ouvert"== listRestaurant?.get(position)?.statut ) {
            viewItem.statut!!.setText("Ouvert")
            viewItem.statut!!.setBackgroundResource(R.drawable.shape_rect07)
            viewItem.statut!!.setTextColor(R.color.colorGreen)


        } else {
            viewItem.statut!!.setBackgroundResource(R.drawable.shap_rect06)
            viewItem.statut!!.setText("ferme")
            viewItem.statut!!.setTextColor(R.color.colorRed)
        }
       /// viewItem.note!!.rating(listRestaurant?.get(position)?.note)

        var  route=context.getResources().getIdentifier("drawable/"+listRestaurant?.get(position)?.photo, null, context.getPackageName());

        viewItem.photo!!.setImageResource(route)

        return convertView
    }

    init {
        listRestaurant = Restaurant
        this.context = context
    }
}



internal class ViewItem {
    var nom: TextView? = null
    var emplacement: TextView? = null
    var specialite: TextView? = null
    //var note: RatingBar? = null
    var description: TextView? = null
    var statut: TextView? = null
    var photo: ImageView? = null
}
