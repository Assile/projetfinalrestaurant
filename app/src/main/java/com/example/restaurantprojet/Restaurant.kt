package com.example.restaurantprojet

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize

@Parcelize
 data class Restaurant(
    var nom: String?, var emplacement: String?, var specialite: String?, var description: String?, var statut: String?, var photo: String?,var adresse:String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        nom =parcel.readString(),
        emplacement =parcel.readString(),
        specialite =parcel.readString(),
        description =parcel.readString(),
        statut =parcel.readString(),
        photo =parcel.readString(),
        adresse =parcel.readString()
    ) {
    }

    companion object : Parceler<Restaurant> {

        override fun Restaurant.write(parcel: Parcel, flags: Int) {
            parcel.writeString(nom)
            parcel.writeString(emplacement)
            parcel.writeString(specialite)
            parcel.writeString(description)
            parcel.writeString(statut)
            parcel.writeString(photo)
            parcel.writeString(adresse)
        }

        override fun create(parcel: Parcel): Restaurant {
            return Restaurant(parcel)
        }
    }
}
