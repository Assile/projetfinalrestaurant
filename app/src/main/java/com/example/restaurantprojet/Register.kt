package com.example.restaurantprojet

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class Register : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_register)
       val btn_send = findViewById<Button>(R.id.register)
        val et_nom = findViewById<EditText>(R.id.Regnom)
        val et_prenom = findViewById<EditText>(R.id.RegPrenom)
        val et_user = findViewById<EditText>(R.id.RegUser)
        val  et_pw = findViewById<EditText>(R.id.RegMdp)
        val   et_mail = findViewById<EditText>(R.id.RegEmail)
       val queue = MySingleton.getInstance(this).requestQueue
        val request = GetWayUser(this, queue)
        btn_send.setOnClickListener {
            val nom: String = et_nom.getText().toString().trim({ it <= ' ' })
            val prenom: String = et_prenom.getText().toString().trim({ it <= ' ' })
            val user: String = et_user.getText().toString().trim({ it <= ' ' })
            val pw: String = et_pw.getText().toString().trim({ it <= ' ' })
            val mail: String = et_mail.getText().toString().trim({ it <= ' ' })
            request.register(nom, prenom, user, pw, mail, object : GetWayUser.RegCallBack {
                override fun onSuccess(message: String?) {
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    intent.putExtra("Register", message)
                    startActivity(intent)
                    finish()
                }
            })
        }
    }

}



