package com.example.restaurantprojet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn_login=findViewById<Button>(R.id.btn_logAcceuil)
        val btn_register=findViewById<Button>(R.id.btn_RegisterAcceuil)

        btn_register?.setOnClickListener{
            val intent=Intent(this,Register::class.java)
            startActivity(intent)
        }
        btn_login?.setOnClickListener{
            val intent =Intent(this,Login::class.java)
            startActivity(intent)
        }


        }
    }
