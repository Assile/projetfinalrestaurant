package com.example.restaurantprojet

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val btn_login = findViewById<Button>(R.id.btn_login)
        val et_user = findViewById<EditText>(R.id.LogUser)
        val et_pw = findViewById<EditText>(R.id.LogPw)

        val queue = MySingleton.getInstance(this).requestQueue
        val request = GetWayUser(this, queue)
         btn_login.setOnClickListener {


             val user: String = et_user.getText().toString().trim({ it <= ' ' })
            val pw: String = et_pw.getText().toString().trim({ it <= ' ' })

            request.connexion(user, pw, object : GetWayUser.LoginCallback {
                override fun onSuccess(user: User) {

                     var session =Session(applicationContext)
                    session.setUserRef(user.user)
                    Toast.makeText(getApplicationContext(),session.getUserRef(),Toast.LENGTH_SHORT).show();

                    val intent = Intent(getApplicationContext() ,Index::class.java)
                    intent.putExtra("User",user)
                    startActivity(intent)
                    finish()
                }

                override fun Onerror(message: String?) {
                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

                }


            })
        }
    }
}