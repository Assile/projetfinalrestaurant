package com.example.restaurantprojet

import android.content.Context
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class GetWayUser constructor(context: Context){
    private var context: Context? = null
    private var queue: RequestQueue? = null
init {
    this.context=context;

}
    constructor(context: Context,queue: RequestQueue) : this(context) {
        this.queue=queue;
    }

    fun register(nom: String, prenom: String, user: String, pw: String, mail: String, callBack: RegCallBack
    ) {


        val url = "http://10.0.0.12/LaboResto/Register.php"
        val request: StringRequest = object : StringRequest(Method.POST, url, Response.Listener {
                val map: Map<String, String> = HashMap()
                callBack.onSuccess("bien joue")
            },
            Response.ErrorListener { }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val map: MutableMap<String, String> =
                    HashMap()
                map["nom"] = nom
                map["prenom"] = prenom
                map["user"] = user
                map["pw"] = pw
                map["mail"] = mail
                return map
            }
        }
        queue!!.add(request)

    }

    interface RegCallBack {
        fun onSuccess(message: String?)
    }

    fun connexion(user: String, pw: String, callback: LoginCallback
    ) {
        val url = "http://10.0.0.12/LaboResto/login.php"
        val request: StringRequest = object : StringRequest(Method.POST, url, Response.Listener { response -> Log.d("APP", response!!)
                var json: JSONObject? = null
                try {
                    json = JSONObject(response)
                    val error = json.getBoolean("error")
                    if (!error) {
                      var  user=User(json.getString("nom"),json.getString("prenom"),json.getString("user"),json.getString("mdp"),json.getString("mail"),json.getString("photo"))

                        callback.onSuccess( user)
                    } else {
                        callback.Onerror(json.getString("message"))
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val map: MutableMap<String, String> =
                    HashMap()
                map["user"] = user
                map["pw"] = pw
                return map
            }
        }
        queue!!.add(request)
    }

    interface LoginCallback {
        fun onSuccess( user:User)
        fun Onerror(message: String?)
    }


}