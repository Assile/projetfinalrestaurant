package com.example.restaurantprojet

import android.content.Context
import com.android.volley.AuthFailureError
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class GetWayRestaurant constructor(context: Context) {
    private var context: Context? = null
    private var queue: RequestQueue? = null

    init {
        this.context = context;

    }

    constructor(context: Context, queue: RequestQueue) : this(context) {
        this.queue = queue;
    }

    fun getAllRestaurant(
        emplacement: String,
        callback: getAllCallback) {

        val listeOfRestaurant: ArrayList<Restaurant> = ArrayList<Restaurant>()
        val url = "http://10.0.0.12/LaboResto/selectALlEmplace.php"
        val request: StringRequest = object : StringRequest(Method.POST, url, Response.Listener { response ->
                var jsonArr: JSONArray? = null





                try {

                    jsonArr = JSONArray(response)
                    var json: JSONObject? = null
                    for (i in 0 until jsonArr.length()) {
                        json = jsonArr.getJSONObject(i)

                        val rest = Restaurant(
                            json.getString("Nom"),
                            json.getString("Emplacement"),
                            json.getString("Specialite"),
                            json.getString("description"),
                            json.getString("statut"),
                            json.getString("photo"),
                            json.getString("adresse")

                        )
                        listeOfRestaurant.add(rest)
                    }
                    callback.onSuccess(listeOfRestaurant)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { }){

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val map: MutableMap<String, String> =
                    HashMap()
                map["emplacement"] = emplacement

                return map
            }
        }
        queue!!.add(request)
    }
    fun GetWayRestaurant(applicationContext: Context?) {
        context = applicationContext
    }

    interface getAllCallback {
        fun onSuccess(listeOfRestaurant: ArrayList<Restaurant>)
        fun Onerror(message: String?)
    }
}
